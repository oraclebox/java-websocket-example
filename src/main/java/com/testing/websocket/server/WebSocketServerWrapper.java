package com.testing.websocket.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.websocket.DeploymentException;
import org.glassfish.tyrus.server.Server;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;

public class WebSocketServerWrapper {
	public static void main(String args[]) {
		runServer();
	}

	private static void runServer() {
		Server server = new Server("localhost", 8080, "",
				EchoWebSocketEndPoint.class);
		
		try {
			server.start();
			System.out.println("Container buffer : "+server.getServerContainer().getDefaultMaxTextMessageBufferSize());
			System.out.println("Container timeout : "+server.getServerContainer().getDefaultMaxSessionIdleTimeout());

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.print("Please press a key to stop the server.");
			Scanner scanner = new Scanner(System.in);
			while (scanner.hasNext()) {
				String input = scanner.nextLine();
				if ("exit".equals(input)) {
					break;
				}
			}
		} catch (DeploymentException e) {
			e.printStackTrace();
		} finally {
			server.stop();
		}
	}
}