package com.testing.websocket.server;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/websocket")
public class EchoWebSocketEndPoint {
	private static List<Session> connectedSessions = new LinkedList<>();
	private Map<String, Object> properties;

	@OnOpen
	public void onOpen(Session session, EndpointConfig config)
			throws IOException {
		System.out.println("Server Buffer size:"
				+ session.getMaxTextMessageBufferSize());
		System.out.println("Server Timeout:" + session.getMaxIdleTimeout());

		// session.setMaxIdleTimeout(10000);
		session.getBasicRemote().sendText("Server onOpen");
		// final Session copySession = session;
		properties = config.getUserProperties();
		// new Thread(new Runnable() {
		// @Override
		// public void run() {
		// ByteBuffer buffer = ByteBuffer.allocate(1);
		// buffer.put((byte) 0xFF);
		// try {
		// while(true) {
		// copySession.getBasicRemote().sendPing(buffer);
		// System.out.println("ping");
		// Thread.sleep(2000);
		// }
		// } catch (IOException | InterruptedException e) {
		// e.printStackTrace();
		// }
		// }
		// }).start();
	}

	@OnMessage
	public void getMessage(final String message, final Session session) {
		if (!connectedSessions.contains(session)) {
			connectedSessions.add(session);
			// broadcastToAll("user" + session.getId() + " joined");
		} else {
			System.out.println("\nReceived message length " + message.length());
			System.out.println("\nReceived message from client "
					+ session.getId() + " :" + message);
			broadcastToAll("user" + session.getId() + " says > " + message);
		}
	}

	private void broadcastToAll(final String s) {
		for (Session session : connectedSessions) {
			try {
				session.getBasicRemote().sendText(s);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@OnClose
	public void closeConnectionHandler(Session session, CloseReason closeReason) {
		System.out.println(closeReason.getCloseCode().getCode());
		System.out.println(closeReason.getReasonPhrase());
		connectedSessions.remove(session);
		broadcastToAll("user" + session.getId() + " quit");
	}

	@OnError
	public void error(Session session, Throwable t) {
		connectedSessions.remove(session);
		broadcastToAll("user" + session.getId() + " error");
		System.out.println(session.getId() + " has error.");
		t.printStackTrace();
	}

}