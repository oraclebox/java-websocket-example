package com.testing.websocket.client;

import java.io.IOException;
import java.nio.ByteBuffer;

import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.PongMessage;
import javax.websocket.Session;

public class ClientWebSocketEndpoint extends Endpoint {
	@Override
	public void onOpen(final Session session, EndpointConfig config) {
		try {
			session.getBasicRemote().sendText("Joining");
		} catch (IOException e) {
			e.printStackTrace();
		}
		session.addMessageHandler(new MessageHandler.Whole<String>() {
			@Override
			public void onMessage(String message) {
				System.out.println(message);
			}

			public void onMessage(PongMessage pongMessage) {
				System.out.println(pongMessage);
			}
		});
		new Thread(new Runnable() {
			@Override
			public void run() {
				ByteBuffer buffer = ByteBuffer.allocate(1);
				buffer.put((byte) 0xFF);
				try {
					session.getBasicRemote().sendPing(buffer);
					Thread.sleep(2000);
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	@OnClose
	public void onClose(Session session, CloseReason closeReason) {
		System.out.println("Server closed session");
		System.out.println("Reason: " + closeReason.getReasonPhrase());
		try {
			session.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@OnError
	public void onError(Session session, Throwable thr) {
		thr.printStackTrace();
		try {
			session.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
